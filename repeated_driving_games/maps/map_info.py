'''
Created on Mar 5, 2021

@author: author
'''

import math

class NYCMapInfo:
    
    vehicle_lane = [(0,4), (13.5,4), (13.5,24), (6.5,24), (6.5,12), (0,12), (0,4)]
    ped_lane = [(3,4), (6.5,4), (6.5,12), (3,12), (3,4)]
    veh_centerline = [(8.25,24), (8.54,13.0),(7.46,11.09), (5.38,10.37), (0,10)]
    ped_centerline = [(4.75,3),(4.75,8),(4.75,16)]
    
    def get_max_stop_dist(self,p):
        
        return math.hypot(p[0]-NYCMapInfo.veh_centerline[2][0],p[1]-NYCMapInfo.veh_centerline[2][1])



class IntersectionClearanceMapInfo:
    
    st2_waypoints = [(538796.97, 4814055.18), (538814.18, 4814030.12), (538818.07, 4814025.05), (538820.96, 4814020.6), (538846.4, 4813982.36)]
    st2_waypoint_segments = ['ln_n_3', 'ln_n_3', 'ln_n_3', 'l_n_s_r', 'ln_s_-2']
    st2_on_intersection_distance = (45,70)
    
    st1_waypoints = [(538810.51, 4814034.38), (538822.99, 4814017.66), (538824.65, 4814015.29), (538826.91, 4814012.06), (538829.43, 4814008.4), (538831.76, 4814004.96), (538834.21, 4814001.28), (538836.99, 4813997.07), (538839.34, 4813993.49), (538841.51, 4813990.19), (538843.84, 4813986.63)]
    st1_waypoint_segments = ['ln_n_3', 'l_n_s_r', 'l_n_s_r', 'l_n_s_r', 'l_n_s_r', 'l_n_s_r', 'l_n_s_r', 'l_n_s_r', 'l_n_s_r', 'ln_s_-2', 'ln_s_-2']
    st1_on_intersection_distance = (20,45)
    
    lt_waypoints = [(538841.75, 4814001.62), (538840.82, 4814003), (538839.06, 4814005.3), (538835.9, 4814007.9), (538831.19, 4814009.78), (538824.85, 4814009.83), (538816.67, 4814007.44), (538807.52, 4814003.24), (538797.61, 4813998.1), (538787.83, 4813992.66)]
    lt_waypoint_segments = ['prep-turn_s', 'prep-turn_s', 'exec-turn_s', 'exec-turn_s', 'exec-turn_s', 'exec-turn_s', 'exec-turn_s', 'ln_w_-1', 'ln_w_-1', 'ln_w_-1']
    

class MergeBeforeIntersection:
    
    ol_waypoints = [(538851.82, 4813986.46), (538846.26, 4813995.81), (538839.95, 4814003.67), (538828.25, 4814009.19), (538815.27, 4814008.59), (538798.87, 4814000.95), (538785.97,4813994.55)]
    ol_waypoint_segments = ['ln_s_1','prep-turn_s', 'prep-turn_s', 'exec-turn_s', 'ln_w_-2', 'ln_w_-2', 'ln_w_-2']
    
    mv_waypoints = [(538855.55, 4813988.15), (538846.26, 4813995.81), (538839.95, 4814003.67), (538828.25, 4814009.19), (538815.27, 4814008.59), (538798.87, 4814000.95), (538785.97,4813994.55)]
    mv_waypoint_segments = ['ln_s_2','prep-turn_s', 'prep-turn_s', 'exec-turn_s', 'ln_w_-2', 'ln_w_-2', 'ln_w_-2']
    
    stopline = [(538833.40, 4813990.81), (538859.79, 4814005.41)]
    
class ParkingPullout:
    
    pv_waypoints = [(538618.28,4812705.98),(538614.70,4812709.96),(538613.20,4812717.39),(538611.46,4812726.04),(538609.71,4812734.69),(538607.96,4812743.01)]
    pv_waypoint_segments = ['ln_s_1','ln_s_1','ln_s_1','ln_s_1','ln_s_1','ln_s_1']
    
    st_waypoints = [(538621.54,4812673.68),(538614.70,4812709.96),(538607.96,4812743.01)]
    st_waypoint_segments = ['prep-turn_s','exec-turn_s','ln_s_1']
    
    img_extent = [538566, 538666, 4812664, 4812750]
    
    exitline = [(538611.31,4812709.16),(538617.94,4812710.20)]
    

class NYCPedVeh:
    
    ped_sn_waypoints = [(598793.7427411242, 4512562.729222552),(598788.1589271266, 4512573.905323617)]
    ped_ns_waypoints = [(598787.9490642876, 4512572.661860691),(598792.2688349555, 4512562.878745487)]
    ped_waypoint_segments = None
    
    veh_nw_waypoints =  [(598788.4380935304, 4512588.912116242), (598793.0388858903, 4512577.297676138), (598793.7013659354, 4512574.317640107), (598792.5443138602, 4512571.933665931), (598789.7382869732, 4512570.091636005), (598765.9127764904, 4512556.746900155)]
    veh_nw_waypoint_segments = ['prep-turn_n','prep-turn_n','prep-turn_n','exec-turn_n','exec-turn_n','ln_w_-1']
    
    veh_sw_waypoints = [ (598808.005302112, 4512561.144856722), (598805.609183224, 4512564.381978391), (598801.1588841947, 4512566.973126278), (598796.6089226431, 4512568.547855811), (598791.6823031097, 4512568.651324648), (598787.2808939241, 4512567.577529044), (598781.3380264542, 4512565.355246456), (598765.4513814254, 4512555.2656383095)]
    veh_sw_waypoint_segments = ['ln_s_1','prep-turn_s','exec-turn_s','exec-turn_s','exec-turn_s','ln_w_-1','ln_w_-1','ln_w_-1']
    
    img_extent = [(598733.6554953968, 4512594.100682354), (598734.2345282246, 4512542.169657248), (598817.8327745282, 4512543.510091318), (598816.9791741266, 4512594.760739151), (598733.6554953968, 4512594.100682354)]