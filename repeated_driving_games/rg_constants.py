'''
Created on May 12, 2021

@author: 
'''
import constants
import os
import sys

DATASET = None
SCENE_TYPE = ('REAL',None)
PROJECT_HOME = 'G:\\code_and_data'
DATA_HOME = os.path.join(PROJECT_HOME,'data','repeated_games_data')
UNI_WEBER_DB_HOME = "G:\\dataset"
SCENE_OUT_PATH = os.path.join(DATA_HOME,'intersection_dataset','scenario_files.csv') if SCENE_TYPE[0] == 'REAL' else os.path.join(DATA_HOME,'SCENE_TYPE[0]','+SCENE_TYPE[1]','scenario_files.csv')
FAILED_FILES_PATH = os.path.join(DATA_HOME,'intersection_dataset','failed_scenario_files.csv') if SCENE_TYPE[0] == 'REAL' else os.path.join(DATA_HOME,'intersection_dataset',SCENE_TYPE[0],SCENE_TYPE[1],'failed_scenario_files.csv')
TREE_FILES = os.path.join(DATA_HOME,'intersection_dataset','game_trees') if SCENE_TYPE[0] == 'REAL' else os.path.join(DATA_HOME,'intersection_dataset'+SCENE_TYPE[0],SCENE_TYPE[1],'game_trees')
RESULTS_FILES = os.path.join(DATA_HOME,'intersection_dataset','results') if SCENE_TYPE[0] == 'REAL' else os.path.join(DATA_HOME,'intersection_dataset',SCENE_TYPE[0],SCENE_TYPE[1],'results')

CURRENT_RG_FILE_ID = None

PROCEED_VEL_RANGES = {'prep-left-turn':(0.5,12),
                      'exec-left-turn':(0.5,12),
                      'prep-right-turn':(0.5,8),
                      'exec-right-turn':(0.5,8),
                      'exit-lane':(8,17),
                      'left-turn-lane':(5,17),
                      'through-lane-entry':(5,17),
                      'through-lane':(5,17),
                      'right-turn-lane':(5,17)}
def get_db_path(file_id = None):
    uni_weber_db_fileid = constants.CURRENT_FILE_ID if file_id is None else file_id
    uni_weber_dbpath = os.path.join(UNI_WEBER_DB_HOME,'intsc_data_'+uni_weber_db_fileid+'.db')
    return uni_weber_dbpath

def get_rg_db_path(file_id):
    if DATASET == 'intersection_dataset':
        if SCENE_TYPE[0] == 'REAL':
            return os.path.join(DATA_HOME,'intersection_dataset','db_files',file_id+'.db')
        else:
            return os.path.join(DATA_HOME,'intersection_dataset',SCENE_TYPE[0],SCENE_TYPE[1],'db_files',file_id+'.db')
    elif DATASET == 'inD':
        if SCENE_TYPE[0] == 'REAL':
            return os.path.join(DATA_HOME,'rg_ind_run','db_files',file_id+'.db')
        else:
            return os.path.join(DATA_HOME,'rg_ind_run',SCENE_TYPE[0],SCENE_TYPE[1],'db_files',file_id+'.db')
    else:
        sys.exit()

    
ind_dataset_path = os.path.join(DATA_HOME,'ind_dataset')
ind_run_path = os.path.join(DATA_HOME,'rg_ind_run','db_files')