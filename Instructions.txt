Experiment machine setup:
Processor: Intel Core i7-6800K CPU@ 3.40 GHz; 6 cores, GPU: GeForce GTX1080
Ti, RAM: 32 GB

Steps for reproduction.

Dependencies: The project was built and run on Windows 10, Python 3.6+, and RStudio 1.3 . Dependencies include Anaconda 4.10.3, which should include all python packages used in the project.

1. Download code and data.zip and extract to a local directory.
2. Edit PROJECT_HOME path in code_and_data\repeated_driving_games\rg constants.py to point to the directory where you extracted the project.
3. Execute python.exe .\runner_all_synthetic_scenarios.py from code_and_data\repeated_driving_games folder.

This script reads all the game trees for the simulation runs (packaged with the project under data\repeated_games_data\intersection_dataset\synthetic\nyc_ped_veh\game_trees folder) and generates the results in all results.csv files found under
data\repeated_games_data\intersection_dataset\synthetic\nyc_ped_veh\results and data\repeated_games_data\intersection_dataset\synthetic\nyc_veh_veh\results for the two scenarios.

4. Edit PROJECT_HOME path in the R scripts under code_and_data\repeated_driving_games\r_scripts\, and run each of the scripts to generate the plots included in the paper.